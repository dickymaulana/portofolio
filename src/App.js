import './App.css';

function App() {
  return (
    <>
    {/* <!-- Navigation--> */}
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="#page-top">
                  <div>DM</div>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ms-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                        <li class="nav-item"><a class="nav-link" href="#services">Skills</a></li>
                        <li class="nav-item"><a class="nav-link" href="#portfolio">Projects</a></li>
                        <li class="nav-item"><a class="nav-link" href="#about">About</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        {/* <!-- Masthead--> */}
        <header class="masthead" id='page-top'>
            <div class="container">
                <div class="masthead-heading text-uppercase">Dicky Maulana</div>
                <div class="masthead-subheading">React & React Native Developer</div>
                <div class="mastheadsubheading">Built with React and Bootstrap 5. Hosted on Netlify</div>
                {/* <a class="btn btn-primary btn-xl text-uppercase" href="#services">Tell Me More</a> */}
            </div>
        </header>
        {/* <!-- Services--> */}
        <section class="page-section" id="services">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Skills</h2>
                    <h3 class="section-subheading text-muted">What i use (Dev & Tolls)</h3>
                </div>
                <div class="row text-center">
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/html.png" alt="..." />
                    <h4 class="my-3">HTML</h4>
                    <p class="text-muted">As frontend developer, we must know the structure of the web or app.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/css.png" alt="..." />
                    <h4 class="my-3">CSS</h4>
                    <p class="text-muted">Can't imagine we built an app or web without this language.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/javascript.png" alt="..." />
                    <h4 class="my-3">JavaScript</h4>
                    <p class="text-muted">More dynamic app and web with this language.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/typescript.png" alt="..." />
                    <h4 class="my-3">TypeScript</h4>
                    <p class="text-muted">Almost no bugs about data type.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/react.png" alt="..." />
                    <h4 class="my-3">React</h4>
                    <p class="text-muted">Base development of server-rendered applications.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/reactnative.png" alt="..." />
                    <h4 class="my-3">React native</h4>
                    <p class="text-muted">Simple, one code compile at everything.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/redux.png" alt="..." />
                    <h4 class="my-3">Redux Thunk</h4>
                    <p class="text-muted">Simple, Best for basic state management.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/reduxsaga.png" alt="..." />
                    <h4 class="my-3">Redux Saga</h4>
                    <p class="text-muted">A complex state management.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/reduxpersist.png" alt="..." />
                    <h4 class="my-3">Redux Persist</h4>
                    <p class="text-muted">Known as offline state management.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/bootstrap.png" alt="..." />
                    <h4 class="my-3">Bootstrap</h4>
                    <p class="text-muted">Lovely to create a responsive web.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/vscode.png" alt="..." />
                    <h4 class="my-3">VS Code</h4>
                    <p class="text-muted">Best and easy code editor to use.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/postman.png" alt="..." />
                    <h4 class="my-3">Postman</h4>
                    <p class="text-muted">My priorty for API platform.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/apollographql.png" alt="..." />
                    <h4 class="my-3">Apollo GraphQl</h4>
                    <p class="text-muted">A quick lifecycle system.</p>
                  </div>
                  <div class="col-md-2">
                    <img class="img-skill" src="assets/img/skill/netlify.svg" alt="..." />
                    <h4 class="my-3">Netlify</h4>
                    <p class="text-muted">Turn ur website into online, and most importantly its free to deploy.</p>
                  </div>
                </div>
            </div>
        </section>
        {/* <!-- Portfolio Grid--> */}
        <section class="page-section bg-light" id="portfolio">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">My Projects</h2>
                    <h3 class="section-subheading text-muted">Some of my works.</h3>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-lg-3 col-sm-3 mb-3">
                        {/* <!-- Portfolio item 1--> */}
                        <div class="portfolio-item">
                            <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal1">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                </div>
                                <img class="img-thumbnail-porto" src="assets/img/portfolio/PegadaianDigital.png" alt="..." />
                            </a>
                            <div class="portfolio-caption">
                                <div class="portfolio-caption-heading">Pegadaian Digital</div>
                                <div class="portfolio-caption-subheading text-muted">2022.06 - Present</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 mb-3">
                        {/* <!-- Portfolio item 2--> */}
                        <div class="portfolio-item">
                            <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal2">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                </div>
                                <img class="img-thumbnail-porto" src="assets/img/portfolio/DamoGo.jpeg" alt="..." />
                            </a>
                            <div class="portfolio-caption">
                                <div class="portfolio-caption-heading">DamoGo</div>
                                <div class="portfolio-caption-subheading text-muted">2021.10 - 2022.06</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 mb-3">
                        {/* <!-- Portfolio item 3--> */}
                        <div class="portfolio-item">
                            <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal3">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                </div>
                                <img class="img-thumbnail-porto" src="assets/img/portfolio/deplaza.png" alt="..." />
                            </a>
                            <div class="portfolio-caption">
                                <div class="portfolio-caption-heading">Deplaza</div>
                                <div class="portfolio-caption-subheading text-muted">2021.05 - 2021.010</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {/* <!-- About--> */}
        <section class="page-section" id="about">
            <div class='container'>
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">About Me</h2>
                </div>
                <div class='text-center'>
                  <h4 class="text-uppercase" style={{marginTop:'5%',marginBottom:'2%'}}>Summary</h4>
                  <ul class='container' style={{listStyle: 'none'}}>
                    <li>Ekstensive 1 year work experience as a Frontend Developer.</li>
                    <li>Involved in Various Projects Ecommerce and worked on all the Devices.</li>
                    <li>Novice level experience working with Redux.</li>
                    <li>Created Reusable components.</li>
                    <li>Good understanding and usage of states and props.</li>
                    <li>Implemented EcmaScript (ES6) arrow function, constants, block-scoope variables.</li>
                    <li>Developing a layout of the application and UI/UX designing.</li>
                    <li>Collaborate with a team of developers to create a mobile application run on android and ios platform.</li>
                    <li>Analyzing code and solving the bugs.</li>
                    <li>Developing and maintaining Git repository and establishing a great collaboration among the cross-functional development team.</li>
                    <li>Coding, testing and implementing commits, builds, and updates in React Native.</li>
                    <li>Create Environtment between Android and Ios for API development.</li>
                  </ul>
                </div>
                <div class='text-center'>
                    <h4 class="text-uppercase" style={{marginTop:'5%',marginBottom:'2%'}}>Education</h4>
                    <ul class='container' style={{listStyle: 'none'}}>
                      <li>SDN JatiMekar IX - Bekasi (2003 - 2006).</li>
                      <li>SMP Gula Putih Mataram - Bandar Lampung (2006 - 2009).</li>
                      <li>Universitas Gunadarma - Depok (2011 - 2017).</li>
                    </ul>
                </div>
                <h4 class="text-uppercase text-center" style={{marginTop:'5%',marginBottom:'3%'}}>Career</h4>
                <ul class="timeline">
                    <li>
                        <div class="timeline-image"><img class="rounded-circle img-porto" src="assets/img/about/itnetsup.jpeg" alt="..." /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2015-2020</h4>
                                <h4 class="subheading">First career as IT Network & Support.</h4>
                            </div>
                            <div class="timeline-body">
                              <p class="text-muted">
                              Network support specialists analyze, troubleshoot and evaluate computer network problems. They play an important role in maintaining an organization's networks, such as performing file backups. They help keep the network safe and secure, through both hardware configuration and end-user training.
                              </p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image"><img class="rounded-circle img-porto" src="assets/img/about/covid19.jpeg" alt="..." /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2020</h4>
                                <h4 class="subheading">COVID-19 Effect</h4>
                            </div>
                            <div class="timeline-body">
                              <p class="text-muted">
                                Fired from the company cause of COVID-19 effect to the company business and Jobless around 1 year. Then start to re-learn about programming, especially on mobile application.
                              </p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/about/3.jpg" alt="..." /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2021</h4>
                                <h4 class="subheading">The beginning as Frontend Developer</h4>
                            </div>
                            <div class="timeline-body">
                              <p class="text-muted">
                              Use React Native as my stronger programming language to work as Mobile Developer at Deplaza.
                              </p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/about/4.jpg" alt="..." /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2022</h4>
                                <h4 class="subheading">Declare as Mid Frontend Developer</h4>
                            </div>
                            <div class="timeline-body">
                              <p class="text-muted">
                                Have knowing almost 75% about Frontend Development, and get hire at BUMN (Pegadaian Company).
                              </p>
                              </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>
                                Be Part
                                <br />
                                Of My
                                <br />
                                Story!
                            </h4>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 text-lg-start">Copyright &copy; DickyMaulana.id 2022</div>
                </div>
            </div>
        </footer>
        {/* <!-- Portfolio Modals--> */}
        {/* <!-- Portfolio item 1 modal popup--> */}
        <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="modal-body">
                                    {/* <!-- Project details--> */}
                                    <h2 class="text-uppercase">Pegadaian Digital</h2>
                                    <p class="item-intro text-muted">Is an innovation from Pegadaian in the form of a Mobile Apps application that makes it easier for Indonesian people to carry out various online Pawn transactions, ranging from Pawn transactions, Gold Savings, to applying for Business Loans and Hajj Portion Financing.
</p>
                                    <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/pegadaianapp.png" alt="..." />
                                    <p>It is only 1 month since i join the Pegadaian Digital Team to developing the app. So far i just build the reusable components and resolving the deprecated depedencies.</p>
                                    <ul class="list-inline">
                                        <li>
                                            <strong>Company:</strong>
                                            PT. Pegadaian & PT. Citta Parama Guna as my Outsource company.
                                        </li>
                                        <li>
                                            <strong>Project Category:</strong>
                                            Digital Services Pawnshop Application Development Consultant Services.
                                        </li>
                                    </ul>
                                    <div class="row justify-content-md-center container" style={{marginBottom:'10%'}}>
                                      <div class="col col-lg-2">
                                        <a href='https://play.google.com/store/apps/details?id=com.pegadaiandigital&hl=en&gl=US'>
                                        <img class="img-brand" src="assets/img/portfolio/playstore.svg" alt="..." />
                                        </a>
                                      </div>
                                      <div class="col col-lg-2">
                                        <a href='https://apps.apple.com/us/app/pegadaian-digital/id1350501409'>
                                        <img class="img-brand" src="assets/img/portfolio/appstore.png" alt="..." />
                                        </a>
                                      </div>
                                    </div>
                                    <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                        <i class="fas fa-xmark me-1"></i>
                                        Close Project
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* <!-- Portfolio item 2 modal popup--> */}
        <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="modal-body">
                                    {/* <!-- Project details--> */}
                                    <h2 class="text-uppercase">DamoGo</h2>
                                    <p class="item-intro text-muted">Optimize your kitchen process, from purchasing supplies to forecasting needs, all in one app.</p>
                                    <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/damogoapp.jpeg" alt="..." />
                                    <p>Built some feature like Notes, Catallogue Product, Fix Google Maps Location, Reusable Components and many more..</p>
                                    <ul class="list-inline">
                                        <li>
                                            <strong>Company:</strong>
                                            PT. Solusi Cerdas Nusantara
                                        </li>
                                        <li>
                                            <strong>Project Category:</strong>
                                            A Mobile marketplace app to help people sell and buy perfectly good but unsold foods or vegetables.
                                        </li>
                                    </ul>
                                    <div class="row justify-content-md-center container" style={{marginBottom:'10%'}}>
                                      <div class="col col-lg-2">
                                        <a href='https://play.google.com/store/apps/details?id=com.damogostore'>
                                        <img class="img-brand" src="assets/img/portfolio/playstore.svg" alt="..." />
                                        </a>
                                      </div>
                                      <div class="col col-lg-2">
                                        <a href='https://apps.apple.com/us/app/damogo-partner/id1452046794'>
                                        <img class="img-brand" src="assets/img/portfolio/appstore.png" alt="..." />
                                        </a>
                                      </div>
                                    </div>
                                    <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                        <i class="fas fa-xmark me-1"></i>
                                        Close Project
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* <!-- Portfolio item 3 modal popup--> */}
        <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="modal-body">
                                    {/* <!-- Project details--> */}
                                    <h2 class="text-uppercase">DePlaza</h2>
                                    <p class="item-intro text-muted">is an online store Dropship application in which there are thousands of Dropship Reseller products. The product is provided by a well-known Top Seller in a large marketplace.</p>
                                    <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/deplazaapp.jpeg" alt="..." />
                                    <p>Refactor the code when i came to this Project. Create chat feature, convert to responsive mobile app and many more..</p>
                                    <ul class="list-inline">
                                        <li>
                                            <strong>Company:</strong>
                                            PT Multiartha Prima Sejahtera
                                        </li>
                                        <li>
                                            <strong>Project Category:</strong>
                                            Dropshiper and Reseller App
                                        </li>
                                    </ul>
                                    <div class="row justify-content-md-center container" style={{marginBottom:'10%'}}>
                                      <div class="col col-lg-2">
                                        <a href='https://play.google.com/store/apps/details?id=com.versiondeplaza&hl=en&gl=US'>
                                        <img class="img-brand" src="assets/img/portfolio/playstore.svg" alt="..." />
                                        </a>
                                      </div>
                                    </div>
                                    <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                        <i class="fas fa-xmark me-1"></i>
                                        Close Project
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
  );
}

export default App;
